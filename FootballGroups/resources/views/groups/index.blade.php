@extends('layouts.app')
@section('content')
    <div class="container col-sm-2">
        <div class="card-body">
            <div class="row">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-6">
                            <label for="Group" class="col-sm-3 control-label">Groups: </label>
                        </div>
                        <div class="col-sm-6">
                            <button id="new_group" type="button" class="btn btn-primary btn-sm">New</button>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table table-borderless table-sm table-responsive" id="groups_list">
                @if(count($groups)>0)
                    @foreach ($groups as $group)
                        <tr>
                            <td>
                                <a href="groups/{{$group->name}}" name="{{$group->name}}">
                                    {{$group->name}}
                                </a>
                            </td>
                            <td>
                                <button name="delete_group" value="{{$group->name}}" class="btn btn-danger btn-sm">
                                    Delete
                                </button>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </table>
        </div>
    </div>

    <script type="text/javascript" src="{{ URL::asset('js/groups.js') }}"></script>
@endsection

