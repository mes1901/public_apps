<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Teams extends Model
{
    protected $fillable = ['name', 'group_id'];
    public $timestamps = false;
    protected $table = 'teams';

    public function create($group_name, $team_name){
        $team = new Teams();
        $team->name = $team_name;
        $get_id = (new Groups())->get_group_id($group_name)->id;
        $team->group_id = $get_id;
        $team->save();
    }

    public function get_teams($id)
    {
        return static::where('group_id', $id)->get();
    }
    public function get_team_id($name)
    {
        return static::where('name', $name)->first('id');
    }

    public function validation($group_name, $team_name)
    {
        $group_id = (new Groups())->get_group_id($group_name)->id;
        $teams_list = static::get_teams($group_id);

        foreach ($teams_list as $team) {
            if ($team->name == $team_name) {
                return false;
            }
        }
        return true;
    }

    /* public static function notActive(){
         return static::where('active',0)->get();
     }*/
}
