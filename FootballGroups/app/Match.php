<?php
/**
 * Created by PhpStorm.
 * User: Eduard
 * Date: 23.05.2019
 * Time: 20:10
 */

namespace App;


class Match
{
    public $first_team_name;
    public $first_team_score;
    public $second_team_name;
    public $second_team_score;
    public $group_name;

    public function get_team_name($id){
        return Teams::where('id',$id)->first('name');
    }

    public function get_group_name($id){
        return Groups::where('id',$id)->first('name');
    }

    public function init($first_name, $second_name, $first_score, $second_score, $group){
        $this->first_team_name = $first_name;
        $this->second_team_name = $second_name;
        $this->first_team_score = $first_score;
        $this->second_team_score = $second_score;
        $this->group_name = $group;
    }
}