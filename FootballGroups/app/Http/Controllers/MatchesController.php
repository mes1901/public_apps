<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class MatchesController extends Controller
{
    public function add_matches(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $teams_playing = $data['playing_teams'];
        $free_to_play = $data['free_to_play'];
        $all_teams = array_merge($free_to_play, $teams_playing);
        $group_id = (new App\Groups())->get_group_id($data['group_name'])->id;
        $matchId = [];
        for ($i = 0; $i < count($free_to_play); $i++) {
            for ($j = $i + 1; $j < count($all_teams); $j++) {
                $match = new App\Matches();
                $match->first_team_id = (new App\Teams())->get_team_id($free_to_play[$i])->id;
                $match->second_team_id = (new App\Teams())->get_team_id($all_teams[$j])->id;
                $match->group_id = $group_id;
                $match->save();
                array_push($matchId, $match->id);
            }
        }
        $matches = App\Matches::find($matchId);
        $data_matches = [];
        foreach ($matches as $match) {
            $data = new App\Match();
            $first_team_name = $data->get_team_name($match->first_team_id);
            $second_team_name = $data->get_team_name($match->second_team_id);
            $group_name = $data->get_group_name($match->group_id);
            $data->init($first_team_name->name, $second_team_name->name, $match->first_team_score, $match->second_team_score, $group_name->name);
            array_push($data_matches, $data);
        }

        $send = json_encode(['matches_created', $data_matches]);
        return $send;
    }
    public function teams_sets(){

    }

    public function active_teams(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        App\Teams::where('name', $data['first_team_name'])->update(['active' => 1]);
        App\Teams::where('name', $data['second_team_name'])->update(['active' => 1]);

        $first_team_id = (new App\Teams())->get_team_id($data['first_team_name'])->id;
        $second_team_id = (new App\Teams())->get_team_id($data['second_team_name'])->id;
        $group_id = (new App\Groups())->get_group_id($data['group_name'])->id;
        App\Matches::where([['first_team_id', $first_team_id], ['second_team_id', $second_team_id], ['group_id', $group_id]])
            ->update(['first_team_score'=> $data['first_team_score'],'second_team_score'=> $data['second_team_score']]);

        $data_teams = [];
        array_push($data_teams, $data['first_team_name'], $data['second_team_name']);
        $send = json_encode(['active_teams', $data_teams]);
        return $send;
    }

    public function inactive_teams(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        App\Teams::where('name', $data['first_team_name'])->update(['active' => 0]);
        App\Teams::where('name', $data['second_team_name'])->update(['active' => 0]);

        $first_team_id = (new App\Teams())->get_team_id($data['first_team_name'])->id;
        $second_team_id = (new App\Teams())->get_team_id($data['second_team_name'])->id;
        $group_id = (new App\Groups())->get_group_id($data['group_name'])->id;
        App\Matches::where([['first_team_id', $first_team_id], ['second_team_id', $second_team_id], ['group_id', $group_id]])
            ->update(['first_team_score'=> $data['first_team_score'],'second_team_score'=> $data['second_team_score']]);

        $data_teams = [];
        array_push($data_teams, $data['first_team_name'], $data['second_team_name']);
        $send = json_encode(['inactive_teams', $data_teams]);
        return $send;
    }
}
