<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;

    public function get_group_id($group_name){
        return static::where('name',$group_name)->first('id');
    }

}
