////////////////////////настройки по подключению к серверу////////////////////////
(function () {
    function getXmlHttp() {
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    }

////////////////////////////отправка данных на сервер и получение ответа////////////////////////////
    function getRequest(type, data, url) {
        console.log(data);
        var request = new getXmlHttp();
        var token = document.querySelector('meta[name="csrf-token"]').content;
        request.open(type, url, true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.setRequestHeader('X-CSRF-TOKEN', token);

        var requestText = null;

        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                requestText = JSON.parse(request.responseText);
                //requestText = request.responseText;
                if (requestText[0] === 'team_created') createTeam(requestText[1]);
                if (requestText[0] === 'team_deleted') deleteTeam(requestText[1], requestText[2]);
                if (requestText[0] === 'matches_created') addMatches(requestText[1]);
                if (requestText[0] === 'active_teams') activeTeams(requestText[1]);
                if (requestText[0] === 'inactive_teams') inactiveTeams(requestText[1]);
            }
        };
        request.send(JSON.stringify(data));
    }

//////////////////////инициализация элементов, кнопка добавления новой команды//////////////////////
    var groupName = document.getElementById('group_name');
    var teamName = document.getElementById('team_name');
    var newTeamButton = document.getElementsByName('add_team')[0];
    var teamsTable = document.getElementById('team_list');
    var matchesTable = document.getElementById('matches_list');

    newTeamButton.addEventListener('click', function () {
        if (teamValidate(teamName.value)) {
            var data = {
                'team_name': teamName.value,
                'group_name': groupName.innerText
            };
            getRequest('POST', data, '/create_new_team');
        }
    });

    ////////////////////////проверка на валидность введенной команды//////////////////////////
    function teamValidate(value) {
        if (value !== '') {
            if (teamsTable.rows.length > 0) {
                var teamList = [];
                for (var i = 0; i < teamsTable.rows.length; i++) {
                    teamList.push(teamsTable.rows[i].cells[0].innerText);
                }
                if (teamList.includes(value)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

//////////////////////////создание команды на стороне клиента/////////////////////////////////
    function createTeam(teamName) {
        var tr = teamsTable.insertRow();
        var td1 = tr.insertCell(0);
        var td2 = tr.insertCell(1);

        td1.innerHTML = teamName;

        var delButton = document.createElement('button');
        delButton.setAttribute('value', teamName);
        delButton.setAttribute('name', 'delete_team');
        delButton.setAttribute('class', 'btn btn-danger btn-sm');
        delButton.innerHTML = "Delete";
        delButton.addEventListener('click', function (e) {
            var tr = e.target.parentNode.parentNode;
            var data = {
                "team_name": e.target.value,
                "group_name": groupName.innerText,
                "id": tr.rowIndex
            };
            getRequest('DELETE', data, '/delete_team');
        });
        td2.appendChild(delButton);
    }

//////////////////////////отправка запроса на сервер по удалению команды////////////////////////////
    var buttons = document.getElementsByName("delete_team");
    buttons.forEach(function (e) {
        e.addEventListener("click", function () {
            var tr = e.parentNode.parentNode;
            var data = {
                "team_name": e.value,
                "group_name": groupName.innerText,
                "id": tr.rowIndex
            };
            getRequest('DELETE', data, '/delete_team');
        });
    });
    /////////////////////////добавляем матч на стороне клиента///////////////////////////
    function addMatches(match) {
        for (var i = 0; i < match.length; i++) {
            var tr = matchesTable.insertRow();
            var td = [];
            for (var j = 0; j < 5; j++) {
                td.push(tr.insertCell(j));
            }
            td[0].innerHTML = match[i]["first_team_name"];
            td[1].appendChild(createInput('first_team_score'));
            var span = document.createElement('span');
            span.innerHTML = ":";
            td[2].appendChild(span);
            td[3].appendChild(createInput('second_team_score'));
            td[4].innerHTML = match[i]["second_team_name"];
        }
    }

    //////////////////////////поля для ввода счета для вновь созданного матча/////////////////////////
    function createInput(id) {
        var input = document.createElement('input');
        input.setAttribute('type', 'text');
        input.setAttribute('name', 'score');
        input.setAttribute('class', 'form-control form-control-sm text-center');
        input.setAttribute('size', '1');
        input.setAttribute('value', '0');
        input.setAttribute('maxlength', '3');
        input.setAttribute('id', id);
        getTeamsScore(input);
        return input;
    }

//////////////////////////удаление команды на стороне клиента///////////////////////////////
    function deleteTeam(getId, matches) {
        teamsTable.rows[getId].remove();
        var rows = matchesTable.rows;
        var count = rows.length;
        for (var i = 0; i < count; i++) {
            rows[0].remove();
        }
        if (matches.length > 0) {
            addMatches(matches);
        }
    }

    ///////////////////////////массив с уникальными значениями//////////////////////////////////
    function arrayDiff(arrayA, arrayB) {
        var result = [];
        for (var i = 0; i < arrayA.length; i++) {
            if (arrayB.indexOf(arrayA[i]) <= -1) {
                result.push(arrayA[i]);
            }
        }
        return result;
    }

    //////////////////////////кнопка генерации матчей с отправкой запроса на сервер///////////////////////////////
    var generateButton = document.getElementsByName('generate_matches')[0];

    generateButton.addEventListener('click', function () {
        var allTeams = getTeamsList();
        var playingTeams = playTeams();
        if (allTeams.length > 1 && allTeams.length > playingTeams.length) {
            var freeToPlay = arrayDiff(allTeams, playingTeams);
            var data = {
                "free_to_play": freeToPlay,
                "playing_teams": playingTeams,
                "group_name": groupName.innerText
            };
            getRequest('POST', data, '/add_matches');

        }
    });
    /////////////////////////////получение списка команд в группе/////////////////////////
    function getTeamsList() {
        var teamList = [];
        for (var i = 0; i < teamsTable.rows.length; i++) {
            teamList.push(teamsTable.rows[i].cells[0].innerText);
        }
        return teamList;
    }

/////////////////////////////список команд которые учавствуют в матчах///////////////////////////
    function playTeams() {
        var unique = [];
        if (matchesTable.rows.length > 0) {
            var matchesList = [];
            for (var i = 0; i < matchesTable.rows.length; i++) {
                matchesList.push(matchesTable.rows[i].cells[0].innerText);
                matchesList.push(matchesTable.rows[i].cells[4].innerText);
            }
            function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
            }

            unique = matchesList.filter(onlyUnique);
        }
        return unique;
    }

///////////////////////////получение счета из матчей////////////////////////////
    var inputs = document.getElementsByName("score");

    inputs.forEach(function (e) {
        getTeamsScore(e);
    });
///////////////////////////////убираем возможность удалить команду, которая играет/////////////////////////////
    function activeTeams(teams) {
        var rows = teamsTable.rows;
        for (var i = 0; i < rows.length; i++) {
            for (var j = 0; j < teams.length; j++) {
                if (rows[i].cells[0].innerText === teams[j]) {
                    var buttonToDel = rows[i].cells[1].getElementsByTagName('button')[0];
                    if (buttonToDel !== undefined) buttonToDel.style.display = "none";
                }
            }
        }
    }

    ///////////////////////////обработка полей со счетом в матчах, для отправки запроса на сервер//////////////////////////
    function getTeamsScore(e) {
        e.addEventListener("keyup", function () {
            var check = false;
            console.log(e.value);
            if (!parseInt(e.value) && (e.value === "")) check = true;
            if (e.value == 0) check = true;
            if (parseInt(e.value)) check = true;
            if (check) {
                var row = e.parentNode.parentNode;
                var currentCell = e.parentNode;
                if (e.value == "") {
                    currentCell.getElementsByTagName('input')[0].value = 0;
                }
                var data;
                var firstCell = row.cells[1].getElementsByTagName('input')[0];
                var secondCell = row.cells[3].getElementsByTagName('input')[0];

                data = {
                    "first_team_name": row.cells[0].innerText,
                    "second_team_name": row.cells[4].innerText,
                    "first_team_score": firstCell.value,
                    "second_team_score": secondCell.value,
                    "group_name": groupName.innerText
                };

                if (firstCell.value == 0 && secondCell.value == 0) {
                    getRequest('POST', data, '/set_inactive_teams');
                } else {
                    getRequest('POST', data, '/set_active_teams');
                }
            }
        });
    }

////////////////////////////////добавляем возможность удаления команд, которые не играют//////////////////////////////////
    function inactiveTeams(teams) {
        var rows = teamsTable.rows;
        for (var i = 0; i < rows.length; i++) {
            for (var j = 0; j < teams.length; j++) {
                if (rows[i].cells[0].innerText === teams[j]) {
                    var buttonToDel = rows[i].cells[1].getElementsByTagName('button')[0];
                    if (buttonToDel !== undefined) buttonToDel.style.display = "block";
                }
            }
        }
    }

})();