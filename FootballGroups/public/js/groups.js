/**
 * Created by Eduard on 21.05.2019.
 */
(function () {
////////////////////////////////настройка по подключению к серверу/////////////////////////
    function getXmlHttp() {
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest !== 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    }

////////////////////////////отправка запроса на сервер и получение ответа////////////////////////////
    function getRequest(type, data, url) {
        var request = new getXmlHttp();
        var token = document.querySelector('meta[name="csrf-token"]').content;
        request.open(type, url, true);
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        request.setRequestHeader('X-CSRF-TOKEN', token);

        var requestText = null;

        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                requestText = JSON.parse(request.responseText);
                if (requestText[0] === 'group_added') addGroup();
                if (requestText[0] === 'group_deleted') deleteGroup(requestText[1]);
            }
        }
        request.send(JSON.stringify(data));

    }

////////////////////////////получение массива с уникальными значениям из двух других массивов////////////////////////////////
    function arrayDiff(arrayA, arrayB) {
        var result = [];
        for (var i = 0; i < arrayA.length; i++) {
            if (arrayB.indexOf(arrayA[i]) <= -1) {
                result.push(arrayA[i]);
            }
        }
        return result;
    }

////////////////////////////////находим не использованное имя группы с помощью ascii//////////////////////////////////
    function createGroup(arr) {
        var numbersFullList = [];
        for (var i = 0; i < 26; i++) {
            numbersFullList.push(i + 1);
        }
        var groupsNumber = [];
        for (var j = 0; j < arr.length; j++) {
            groupsNumber.push((arr[j].charCodeAt(0)) - 64);
        }
        var numbersSorted = arrayDiff(numbersFullList, groupsNumber);
        var lastNumber = groupsNumber[groupsNumber.length - 1];
        var temp = lastNumber + 1;
        while (true) {
            if (temp > 26) temp = 1;
            if (numbersSorted.includes(temp)) {
                return String.fromCharCode(temp + 64);
            }
            temp++;
        }
    }

/////////////////////////////////инициализация полей, кнопка для добавления новой группы/////////////////////////////////////
    var groupsTable = document.getElementById('groups_list');
    var newGroupButton = document.getElementById('new_group');
    var groupName = null;

    newGroupButton.addEventListener('click', function () {
        var groupList = [];
        if (groupsTable.rows.length !== 0 && groupsTable.rows.length !== 26) {
            for (var i = 0; i < (groupsTable.rows).length; i++) {
                var row = groupsTable.rows[i];
                var cell = row.cells[0].getElementsByTagName('a')[0];
                groupList.push(cell.name);
            }
            groupName = createGroup(groupList);
        } else if (groupsTable.rows.length === 0) {
            groupName = "A";
        } else {
            return;
        }
        getRequest('POST', groupName, '/create_new_group');

    });
//////////////////////////////создание группы на стороне клиента///////////////////////////////
    function addGroup() {
        var tr = groupsTable.insertRow();
        var td1 = tr.insertCell(0);
        var td2 = tr.insertCell(1);

        var a = document.createElement('a');
        a.setAttribute('href', '/groups/' + groupName);
        a.setAttribute('name', groupName);
        a.innerHTML = groupName;
        td1.appendChild(a);

        var delButton = document.createElement('button');
        delButton.setAttribute('value', groupName);
        delButton.setAttribute('name', 'delete_group');
        delButton.setAttribute('class', 'btn btn-danger btn-sm');
        delButton.innerHTML = "Delete";
        delButton.addEventListener('click', function (e) {
            var tr = e.target.parentNode.parentNode;
            var data = {
                "name": e.target.value,
                "id": tr.rowIndex
            };
            getRequest('DELETE', data, '/delete_group');
        });
        td2.appendChild(delButton);
    }

////////////////////////////событие на кнопки удаления группы///////////////////////////
    var buttons = document.getElementsByName("delete_group");
    buttons.forEach(function (e) {
        e.addEventListener("click", function () {
            var tr = e.parentNode.parentNode;
            var data = {
                "name": e.value,
                "id": tr.rowIndex
            };
            getRequest('DELETE', data, '/delete_group');

        });
    });
//////////////////////удаление группы из view///////////////////////
    function deleteGroup(getId) {
        groupsTable.rows[getId].remove();
    }

})();
